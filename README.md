# Условие

Система Cеть LikeIT. Пользователь создает сообщения c просьбой о помощи в некотором вопросе, отвечает на сообщения других Пользователей, корректирует свои сообщения и персональную информацию. За ответ на поставленный вопрос, Пользователь ставит оценку по некоторой шкале. Оценки других Пользователей составляют рейтинг конкретного Пользователя. Администратор создает (изменяет, удаляет) темы сообщений, управляет Пользователями.

### Возможности проекта

Проект поддерживает следующие команды:

* Просмотр заданных вопросов (открытых и закрытых - всеми, удаленных - администраторами)
* Поиск вопросов по тегам и названию (всеми)
* Создание новых вопросов (Подтвержденными пользователями)
* Создание ответов на открытые вопросы (Подтвержденными пользователями)
* Оценка существующих ответов (Подтвержденными пользователями)
* Изменение вопросов (Автором)
* Изменение заголовка и тегов вопросов (Администраторами)
* Изменение ответов (Автором)
* Просмотр всех пользователей (Администратором)
* Бан пользователей (Администратором)
* Создание новых администраторов (СуперАдминистратором)
* Удаление администраторов (СуперАдминистратором)
* Изменение локали на любой странице
* Логин
* Создание новых пользователей (с отправкой подтверждающего сообщения на почту)
* Изменение пароля на персональной странице
* Изменение почты на персональной странице (с отправкой подтверждающего сообщения на почту)

### Техническое описание проекта:
* Java 8;
* JavaEE: Servlet, JSP;
* Server / Servlet container: tomcat7-maven-plugin 2.1
* Data base: MySQL;
* JDBC;
* Logger: Log4J;
* Tests: JUnit4;
* Build tool: Maven.
* Mail: Javax.Mail 1.4
* Lombok 1.16.16
* AJAX, JQuery