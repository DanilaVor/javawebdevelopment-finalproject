package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

/**
 * Shows appropriate edit question form
 * Required user status: available, role: any
 * @see CommandEnum
 */
@Log4j2
public class ShowEditQuestionFormCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request params: questionId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic()){
            long id = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            boolean hasRight = questionLogic.hasEditRights(id, user);
            if(hasRight){
                QuestionBean questionBean = questionLogic.getQuestionFull(id, user);
                request.setAttribute(RequestParamAttr.QUESTION, questionBean);
                router.setPage(PathEnum.EDIT_QUESTION);
            }
            else{
                if(userLogic.hasGeneralQuestionEditRights(user)){
                    QuestionBean questionBean = questionLogic.getQuestionFull(id, user);
                    request.setAttribute(RequestParamAttr.QUESTION, questionBean);
                    router.setPage(PathEnum.ADMIN_EDIT_QUESTION);
                }
                else {
                    router.setPage(PathEnum.ERROR_500);
                    router.changeAction();
                }
            }
        }
        catch (NumberFormatException | ProjectException e){
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
