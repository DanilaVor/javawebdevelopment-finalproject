package com.epam.likeit.command.impl;

import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.LocaleUtil;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Changes locale of current session to specified
 * Required user status: any, role: any
 * @see CommandEnum
 */
public class ChangeLocaleCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request parameter: locale
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        String localeString = request.getParameter(RequestParamAttr.LOCALE);
        Locale locale = LocaleUtil.getLocale(localeString);
        Config.set(request.getSession(), Config.FMT_LOCALE, locale);
        router.setReturnBack(true);
        return router;
    }
}
