package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Uploads avatar to server
 * Required user status: available, role: any
 * @see CommandEnum
 */
@Log4j2
public class UploadAvatarCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: basePath (real path of app)
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router page = new Router();
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic()){
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            String basePath = request.getServletContext().getRealPath(File.separator);
            String extension = userLogic.uploadAvatar(user.getId(), basePath, request.getParts());
            user.setAvatarPath(extension);
            UserBean userBean = userLogic.getUser(user.getId());
            List<QuestionBean> questions = questionLogic.getUserQuestions(user.getId(), user);
            request.setAttribute(RequestParamAttr.USERPAGE_USER, userBean);
            request.setAttribute(RequestParamAttr.QUESTIONS, questions);
            page.setPage(PathEnum.OWNER_USERPAGE);
        } catch (ProjectException | ServletException | IOException e) {
            log.error(e);
            page.setPage(PathEnum.ERROR_500);
            page.changeAction();
        }
        return page;
    }
}
