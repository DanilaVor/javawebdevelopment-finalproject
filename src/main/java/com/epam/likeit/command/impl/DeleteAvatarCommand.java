package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;

import java.io.File;

/**
 * Deletes avatar of current user
 * Required user status: any, role: user
 * @see CommandEnum
 */
public class DeleteAvatarCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
        try(UserLogic userLogic = new UserLogic()){
            if(userLogic.isUserAvailable(user)){
                userLogic.deleteAvatar(user, request.getServletContext().getRealPath(File.separator));
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        } catch (ProjectException e) {
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
