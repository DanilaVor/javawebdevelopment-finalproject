package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.validator.UserValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Changes password of current user
 * Required user status: not banned, role: any
 * @see CommandEnum
 */
@Log4j2
public class ChangePasswordCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: passwordOne, passwordTwo
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
        String passwordOne = request.getParameter(RequestParamAttr.PASSWORD_ONE);
        String passwordTwo = request.getParameter(RequestParamAttr.PASSWORD_TWO);
        if(passwordOne != null && passwordOne.equals(passwordTwo)){
            if(UserValidator.validatePassword(passwordOne)) {
                try (UserLogic userLogic = new UserLogic()) {
                    UserBean currentUser = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
                    if (userLogic.isUserAvailable(currentUser.getId())) {
                        userLogic.changePassword(currentUser, passwordOne);
                        router.setPage(PathEnum.INTERMEDIATE);
                        request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
                    } else {
                        router.setPage(PathEnum.ERROR_403);
                        router.changeAction();
                    }
                } catch (ProjectException e) {
                    log.error(e);
                    router.setPage(PathEnum.ERROR_500);
                    router.changeAction();
                }
            }
            else{
                request.getSession().setAttribute(RequestParamAttr.ERROR_MESSAGE_PASSWORD,
                        Message.PASSWORD_INVALID.get(locale));
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
                router.setPage(PathEnum.INTERMEDIATE);
            }
        }
        else{
            request.getSession().setAttribute(RequestParamAttr.ERROR_MESSAGE_PASSWORD,
                    Message.SIGNUP_PASSWORD_MISMATCH.get(locale));
            request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
            router.setPage(PathEnum.INTERMEDIATE);
        }
        return router;
    }
}
