package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.Configuration;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * Show specified page of all users
 * Required user status: any, role: admin
 * @see CommandEnum
 */
@Log4j2
public class ShowUsersCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: page
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic()){
            long page = Long.parseLong(request.getParameter(RequestParamAttr.PAGE));
            long noOfRecords = userLogic.getCount();
            long noOfPages = (int) Math.ceil(noOfRecords * 1.0 / Configuration.USERS_PER_PAGE);
            List<UserBean> list;
            if(noOfPages > 1){
                list  = userLogic.getUsers((page-1)*Configuration.USERS_PER_PAGE, Configuration.USERS_PER_PAGE);
            }
            else{
                list  = userLogic.getUsers(0, Configuration.USERS_PER_PAGE);
                page = 1;
            }
            request.setAttribute(RequestParamAttr.USERS, list);
            request.setAttribute(RequestParamAttr.PAGES, noOfPages);
            request.setAttribute(RequestParamAttr.PAGE, page);
            router.setPage(PathEnum.USERS);
        } catch (ProjectException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
