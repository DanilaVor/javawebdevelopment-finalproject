package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Sends another confirmation message
 * Required user status: not confirmed, role: any
 * @see CommandEnum
 */
public class ResendConfirmationCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic()){
            UserBean userBean = (UserBean)request.getSession().getAttribute(RequestParamAttr.USER);
            boolean userNotConfirmed = userLogic.isUserNotConfirmed(userBean.getId());
            Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            if(userNotConfirmed){
                UserBean loadedUser = userLogic.getUser(userBean.getId());
                StringBuffer baseLink = request.getRequestURL();
                userLogic.trySendConfirmationEmail(loadedUser.getEmail(), locale, baseLink.toString(), userBean);
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        } catch (ProjectException e) {
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
