package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.AnswerLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.validator.AnswerValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Creates answer for specified question
 * Required user status: available, role: any
 * @see CommandEnum
 */
@Log4j2
public class CreateAnswerCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: text, questionId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(AnswerLogic answerLogic = new AnswerLogic()){
            String text = request.getParameter(RequestParamAttr.TEXT).trim();
            long questionId = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            UserBean currentUser = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            if(AnswerValidator.validateText(text)) {
                answerLogic.createAnswer(currentUser.getId(), questionId, text);
            }
            else{
                String message = Message.ANSWER_TEXT_INVALID.get(locale);
                request.getSession().setAttribute(RequestParamAttr.ERROR_MESSAGE_ANSWER, message);
            }
            router.setPage(PathEnum.INTERMEDIATE);
            request.setAttribute(RequestParamAttr.QUESTION_ID, questionId);
            request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTION.name());
        }
        catch (NumberFormatException | ProjectException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
