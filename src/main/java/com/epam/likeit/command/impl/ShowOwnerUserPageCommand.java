package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * Shows owner user page
 * Required user status: any, role: user
 * @see CommandEnum
 */
@Log4j2
public class ShowOwnerUserPageCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic()){
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            UserBean userpageUser = userLogic.getUser(user.getId());
            List<QuestionBean> questions = questionLogic.getUserQuestions(user);
            request.setAttribute(RequestParamAttr.USERPAGE_USER, userpageUser);
            request.setAttribute(RequestParamAttr.QUESTIONS, questions);
            router.setPage(PathEnum.OWNER_USERPAGE);
        } catch (ProjectException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
