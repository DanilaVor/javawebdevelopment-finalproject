package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * Shows user page
 * Required user status: any, role: any
 * @see CommandEnum
 */
@Log4j2
public class ShowUserPageCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: userId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic()){
            long id = Long.parseLong(request.getParameter(RequestParamAttr.USER_ID));
            UserBean currentUser = (UserBean)request.getSession().getAttribute(RequestParamAttr.USER);
            UserBean userBean = userLogic.getUser(id);
            List<QuestionBean> questions = questionLogic.getUserQuestions(id, currentUser);
            request.setAttribute(RequestParamAttr.USERPAGE_USER, userBean);
            request.setAttribute(RequestParamAttr.QUESTIONS, questions);
            router.setPage(PathEnum.USERPAGE);
        }
        catch (ProjectException | NumberFormatException e){
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
        }
        return router;
    }
}
