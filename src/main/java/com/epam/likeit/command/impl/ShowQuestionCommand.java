package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.bean.*;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

/**
 * Shows full question info with answers
 * Required user status: any, role: any
 * @see CommandEnum
 */
@Log4j2
public class ShowQuestionCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: questionId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try (QuestionLogic questionLogic = new QuestionLogic();
             UserLogic userLogic = new UserLogic()){
            long questionId = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            UserBean currentUser = (UserBean)request.getSession().getAttribute(RequestParamAttr.USER);
            if(questionLogic.questionViewAvailable(questionId, currentUser)) {
                QuestionBean question = questionLogic.getQuestionFull(questionId, currentUser);
                request.setAttribute(RequestParamAttr.QUESTION, question);
                if(currentUser != null) {
                    UserBean userBean = userLogic.getUser(currentUser.getId());
                    request.setAttribute(RequestParamAttr.CURRENT_USER, userBean);
                }
                router.setPage(PathEnum.QUESTION);
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        } catch (ProjectException e) {
            log.error("Exception in dao layer", e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        } catch (NumberFormatException e){
            log.error("Question id should be long", e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
