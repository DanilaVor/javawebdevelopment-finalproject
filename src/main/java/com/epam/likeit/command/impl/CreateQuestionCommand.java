package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.TagUtil;
import com.epam.likeit.util.validator.QuestionValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Creates question
 * Required user status: available, role: any
 * @see CommandEnum
 */
@Log4j2
public class CreateQuestionCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: title, tags, text
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        String title = request.getParameter(RequestParamAttr.TITLE).trim();
        String tags = request.getParameter(RequestParamAttr.TAGS).trim();
        String text = request.getParameter(RequestParamAttr.TEXT).trim();
        UserBean currentUser = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
        Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
        router.setPage(PathEnum.CREATE_QUESTION);
        boolean infoValid = true;
        if(!QuestionValidator.validateText(text)){
            infoValid = false;
            request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TEXT, Message.QUESTION_TEXT_INVALID);
        }
        if(!QuestionValidator.validateTitle(title)){
            infoValid = false;
            request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TITLE, Message.QUESTION_TITLE_INVALID);
        }
        TagUtil.ParseResult parsedTags = TagUtil.parseQuestionsTags(tags, locale);
        if(parsedTags.getErrorMessage() != null){
            infoValid = false;
            request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TAGS, parsedTags.getErrorMessage());
        }
        if(infoValid){
            try {
                QuestionLogic questionLogic = new QuestionLogic();
                QuestionBean questionBean = new QuestionBean();
                questionBean.setTitle(title);
                if(!text.isEmpty()){
                    questionBean.setText(text);
                }
                else{
                    questionBean.setText(title);
                }
                questionBean.setTags(parsedTags.getTags());
                questionLogic.createQuestion(questionBean, currentUser);
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTION.name());
                request.setAttribute(RequestParamAttr.QUESTION_ID, questionBean.getId());
            }
            catch (ProjectException e){
                log.error(e);
                router.setPage(PathEnum.ERROR_500);
                router.changeAction();
            }
        }
        else{
            router.setPage(PathEnum.CREATE_QUESTION);
            request.setAttribute(RequestParamAttr.TAGS, tags);
            request.setAttribute(RequestParamAttr.TITLE, title);
            request.setAttribute(RequestParamAttr.TEXT, text);
        }
        return router;
    }
}
