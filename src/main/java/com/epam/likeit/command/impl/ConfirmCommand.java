package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.entity.UserStatus;
import com.epam.likeit.logic.ConfirmationLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Tries to confirm registration
 * Required user status: not confirmed, role: any
 * @see CommandEnum
 */
@Log4j2
public class ConfirmCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request params: code, cid (confirmation id)
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router page = new Router();
        try (UserLogic userLogic = new UserLogic();
             ConfirmationLogic confirmationLogic = new ConfirmationLogic()){
            String code = request.getParameter(RequestParamAttr.CODE);
            Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            long cid = Long.parseLong(request.getParameter(RequestParamAttr.CONFIRMATION_ID));
            if(!confirmationLogic.isExpired(cid)){
                if(userLogic.isUserNotConfirmedByCid(cid)){
                    boolean result = userLogic.tryConfirmRegistration(code, cid);
                    if(result){
                        UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
                        if(user != null && user.getStatus() == UserStatus.NOT_CONFIRMED){
                            user.setStatus(UserStatus.AVAILABLE);
                            request.getSession().setAttribute(RequestParamAttr.USER, user);
                        }
                        page.setPage(PathEnum.INTERMEDIATE);
                        request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTIONS.name());
                        request.getSession().setAttribute(RequestParamAttr.ALERT_SUCCESS_MESSAGE,
                                Message.CONFIRMATION_CONFIRMED.get(locale));
                    }
                    else{
                        page.setPage(PathEnum.ERROR_500);
                        page.changeAction();
                    }
                }
                else{
                    request.getSession().setAttribute(RequestParamAttr.ALERT_ERROR_MESSAGE,
                            Message.CONFIRMATION_ALREADY_CONFIRMED.get(locale));
                    request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTIONS.name());
                    page.setPage(PathEnum.INTERMEDIATE);
                }
            }
            else{
                request.getSession().setAttribute(RequestParamAttr.ALERT_ERROR_MESSAGE,
                        Message.CONFIRMATION_EXPIRED.get(locale));
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTIONS.name());
                page.setPage(PathEnum.INTERMEDIATE);
            }
        } catch (ProjectException | NumberFormatException e) {
            log.error(e);
            page.setPage(PathEnum.ERROR_500);
            page.changeAction();
        }
        return page;
    }
}
