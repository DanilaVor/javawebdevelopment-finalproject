package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.QuestionBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.TagUtil;
import com.epam.likeit.util.validator.QuestionValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Saves changes to question, made by author
 * Required user status: available, role: any
 * @see CommandEnum
 */
@Log4j2
public class SaveQuestionChangesCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request params: title, text, tags, questionId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        String title = request.getParameter(RequestParamAttr.TITLE).trim();
        String tags = request.getParameter(RequestParamAttr.TAGS);
        String text = request.getParameter(RequestParamAttr.TEXT).trim();
        boolean infoValid = true;
        try (QuestionLogic questionLogic = new QuestionLogic()){
            Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            long id = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            boolean hasRight = questionLogic.hasEditRights(id, user);
            if(hasRight){
                if(!QuestionValidator.validateText(text)){
                    infoValid = false;
                    String message = Message.QUESTION_TEXT_INVALID.get(locale);
                    request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TEXT, message);
                }
                if(!QuestionValidator.validateTitle(title)){
                    infoValid = false;
                    String message = Message.QUESTION_TITLE_INVALID.get(locale);
                    request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TITLE, message);
                }
                TagUtil.ParseResult parsedTags = TagUtil.parseQuestionsTags(tags, locale);
                if(parsedTags.getErrorMessage() != null){
                    infoValid = false;
                    request.setAttribute(RequestParamAttr.ERROR_MESSAGE_TAGS, parsedTags.getErrorMessage());
                }
                if(infoValid){
                    QuestionBean questionBean = new QuestionBean();
                    questionBean.setId(id);
                    questionBean.setText(text);
                    questionBean.setTitle(title);
                    questionBean.setTags(parsedTags.getTags());
                    questionLogic.editQuestion(questionBean);
                    router.setPage(PathEnum.INTERMEDIATE);
                    request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTION.name());
                    request.setAttribute(RequestParamAttr.QUESTION_ID, questionBean.getId());
                }
                else {
                    QuestionBean questionBean = questionLogic.getQuestionFull(id, user);
                    questionBean.setText(text);
                    questionBean.setTitle(title);
                    questionBean.setTagsAggregated(tags);
                    request.setAttribute(RequestParamAttr.QUESTION, questionBean);
                    router.setPage(PathEnum.EDIT_QUESTION);
                }
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        }
        catch (NumberFormatException | ProjectException e){
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
