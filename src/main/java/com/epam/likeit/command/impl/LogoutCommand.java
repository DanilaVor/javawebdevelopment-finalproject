package com.epam.likeit.command.impl;

import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.Router;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;

import javax.servlet.http.HttpSession;

/**
 * Logs out
 * Required user status: any, role: any
 * @see CommandEnum
 */
public class LogoutCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        router.changeAction();
        router.setPage(PathEnum.INDEX);
        HttpSession session = request.getSession(false);
        if(session != null) {
            session.invalidate();
        }
        return router;
    }
}
