package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

/**
 * Closes question
 * Required user status: available, role: admin
 * @see CommandEnum
 */
@Log4j2
public class CloseQuestionCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: question id
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(QuestionLogic questionLogic = new QuestionLogic()){
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            long questionId = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            boolean hasCloseRights = questionLogic.hasCloseRights(questionId, user);
            if(hasCloseRights){
                questionLogic.closeQuestion(questionId);
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTION);
                request.setAttribute(RequestParamAttr.QUESTION_ID, questionId);
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        } catch (ProjectException | NumberFormatException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
