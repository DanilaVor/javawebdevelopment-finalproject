package com.epam.likeit.command.impl;

import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.Router;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;

/**
 * Shows login form
 * Required user status: any, role: any
 * @see CommandEnum
 */
public class ShowLoginCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        router.setPage(PathEnum.LOGIN);
        return router;
    }
}
