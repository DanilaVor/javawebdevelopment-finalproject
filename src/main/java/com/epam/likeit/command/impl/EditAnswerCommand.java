package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.AnswerBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.AnswerLogic;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EditAnswerCommand implements ActionCommand {
    /**
     * {@inheritDoc}
     * Required request params: questionId, answerId, text
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        UserBean currentUser = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic();
            AnswerLogic answerLogic = new AnswerLogic()){
            long questionId = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            long answerId = Long.parseLong(request.getParameter(RequestParamAttr.ANSWER_ID));
            String text = request.getParameter(RequestParamAttr.TEXT).trim();
            boolean userAvailable = userLogic.isUserAvailable(currentUser.getId());
            boolean questionAvailable = questionLogic.answerActionAvailable(questionId);
            boolean answerAvailable = answerLogic.answerEditAvailable(answerId, currentUser);
            if(userAvailable && questionAvailable && answerAvailable){
                AnswerBean answerBean = new AnswerBean();
                answerBean.setId(answerId);
                answerBean.setText(text);
                answerLogic.edit(answerBean);
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND.toLowerCase(), CommandEnum.SHOW_QUESTION.name());
                request.setAttribute(RequestParamAttr.QUESTION_ID, questionId);
            }
            else{
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        }
        catch (ProjectException | NumberFormatException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
