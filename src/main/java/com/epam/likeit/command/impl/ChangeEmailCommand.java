package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import com.epam.likeit.util.validator.UserValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

/**
 * Changes email of current user and sends confirmation email
 * Required user status: not banned, role: any
 * @see CommandEnum
 */
@Log4j2
public class ChangeEmailCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request params: email;
     * Required session attributes: user;
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        String email = request.getParameter(RequestParamAttr.EMAIL);
        Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
        if(UserValidator.validateEmail(email)) {
            try (UserLogic userLogic = new UserLogic()) {
                UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
                if(userLogic.checkEmailAvailable(email)){
                    user.setEmail(email);
                    userLogic.changeEmail(user, locale, request.getRequestURL().toString());
                    router.setPage(PathEnum.INTERMEDIATE);
                    request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
                }
                else{
                    request.getSession().setAttribute(RequestParamAttr.ERROR_MESSAGE_EMAIL,
                            Message.SIGNUP_EMAIL_TAKEN.get(locale));
                    request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
                    router.setPage(PathEnum.INTERMEDIATE);
                }
            } catch (ProjectException e) {
                log.error(e);
                router.setPage(PathEnum.ERROR_500);
                router.changeAction();
            }
        }
        else{
            request.getSession().setAttribute(RequestParamAttr.ERROR_MESSAGE_EMAIL, Message.EMAIL_INVALID.get(locale));
            request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_OWNER_USER_PAGE.name());
            router.setPage(PathEnum.INTERMEDIATE);
        }
        return router;
    }
}
