package com.epam.likeit.command.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.ActionCommand;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.command.Router;
import com.epam.likeit.logic.AnswerLogic;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.resource.PathEnum;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

/**
 * Deletes specified answer
 * Required user status: available, role: admin
 * @see CommandEnum
 */
@Log4j2
public class DeleteAnswerCommand implements ActionCommand{
    /**
     * {@inheritDoc}
     * Required request params: questionId, answerId
     * Required session attributes: user
     */
    @Override
    public Router execute(RequestWrapper request) {
        Router router = new Router();
        try(UserLogic userLogic = new UserLogic();
            QuestionLogic questionLogic = new QuestionLogic();
            AnswerLogic answerLogic = new AnswerLogic()){
            UserBean userBean = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            long questionId = Long.parseLong(request.getParameter(RequestParamAttr.QUESTION_ID));
            long answerId = Long.parseLong(request.getParameter(RequestParamAttr.ANSWER_ID));
            boolean userHasRights = userLogic.hasDeleteRights(userBean);
            boolean answerActionsAvailable = questionLogic.answerActionAvailable(questionId);
            if(userHasRights && answerActionsAvailable){
                answerLogic.delete(answerId);
                router.setPage(PathEnum.INTERMEDIATE);
                request.setAttribute(RequestParamAttr.COMMAND, CommandEnum.SHOW_QUESTION);
                request.setAttribute(RequestParamAttr.QUESTION_ID, questionId);
            }
            else {
                router.setPage(PathEnum.ERROR_403);
                router.changeAction();
            }
        } catch (ProjectException | NumberFormatException e) {
            log.error(e);
            router.setPage(PathEnum.ERROR_500);
            router.changeAction();
        }
        return router;
    }
}
