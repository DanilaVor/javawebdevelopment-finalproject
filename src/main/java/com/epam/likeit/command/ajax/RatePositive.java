package com.epam.likeit.command.ajax;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.AnswerBean;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.AjaxCommand;
import com.epam.likeit.command.AjaxCommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.logic.AnswerLogic;
import com.epam.likeit.logic.QuestionLogic;
import com.epam.likeit.resource.Message;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.jstl.core.Config;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Puts positive mark to specified answer
 * Required user status: available, role: any
 * @see AjaxCommandEnum
 */
@Log4j2
public class RatePositive implements AjaxCommand {
    /**
     * {@inheritDoc}
     * Required request params: userId, answerId
     * Required session attributes: user
     */
    @Override
    public Map<String, String> execute(RequestWrapper request) {
        Map<String, String> dataMap = new HashMap<>();
        try(QuestionLogic questionLogic = new QuestionLogic();
            AnswerLogic answerLogic = new AnswerLogic()) {
            UserBean user = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            long userId = user.getId();
            long answerId = Long.parseLong(request.getParameter(RequestParamAttr.ANSWER_ID));
            AnswerBean answerBean = answerLogic.get(answerId);
            if(!answerBean.isDeleted() && questionLogic.answerActionAvailable(answerBean.getQuestionId())) {
                answerBean = answerLogic.ratePositive(answerId, userId);
                dataMap.put(RequestParamAttr.POSITIVE, String.valueOf(answerBean.getPositiveRating()));
                dataMap.put(RequestParamAttr.NEGATIVE, String.valueOf(answerBean.getNegativeRating()));
                dataMap.put(RequestParamAttr.CURRENT_MARK, String.valueOf(answerBean.getCurrentUserMark()));
            }
            else{
                dataMap.put(RequestParamAttr.ERROR, Message.AJAX_ANSWER_RATE_ERROR.get(locale));
            }
        } catch (ProjectException | NumberFormatException e) {
            log.error(e);
        }
        return dataMap;
    }
}
