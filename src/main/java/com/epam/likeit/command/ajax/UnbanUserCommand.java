package com.epam.likeit.command.ajax;

import com.epam.likeit.ProjectException;
import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.AjaxCommand;
import com.epam.likeit.command.AjaxCommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.logic.UserLogic;
import com.epam.likeit.servlet.RequestWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

/**
 * Unbans specified user
 * Required user status: available, role: admin
 * @see AjaxCommandEnum
 */
@Log4j2
public class UnbanUserCommand implements AjaxCommand{
    /**
     * {@inheritDoc}
     * Required request params: userId
     * Required session attributes: user
     */
    @Override
    public Map<String, String> execute(RequestWrapper request) {
        Map<String, String> dataMap = new HashMap<>();
        try(UserLogic userLogic = new UserLogic()) {
            long userId = Long.parseLong(request.getParameter(RequestParamAttr.USER_ID));
            UserBean currentUser = (UserBean) request.getSession().getAttribute(RequestParamAttr.USER);
            userLogic.unbanUser(currentUser, userId);
            dataMap.put(RequestParamAttr.SUCCESS, RequestParamAttr.SUCCESS_MARKER);
        } catch (ProjectException | NumberFormatException e) {
            log.error(e);
            dataMap.put(RequestParamAttr.SUCCESS, RequestParamAttr.FAILURE_MARKER);
        }
        return dataMap;
    }
}