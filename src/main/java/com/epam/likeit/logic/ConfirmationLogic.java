package com.epam.likeit.logic;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.ConfirmationDao;
import com.epam.likeit.dao.TransactionHelper;
import com.epam.likeit.dao.UserDao;
import com.epam.likeit.entity.Confirmation;

import java.time.Instant;
import java.util.Optional;

/**
 * Class, that provides methods for confirmation commands
 */
public class ConfirmationLogic extends AbstractLogic{
    private UserDao userDao;
    private ConfirmationDao confirmationDao;

    public ConfirmationLogic(){
        userDao = TransactionHelper.getFactory().getUserDao();
        confirmationDao = TransactionHelper.getFactory().getConfirmationDao();
        getHelper().prepareDaos(userDao, confirmationDao);
    }

    /**
     * Checks, if confirmation with specified id is expired
     * @param cid confirmation id
     * @return true, if confirmation has expired, false otherwise
     * @throws ProjectException if confirmation can't be found
     */
    public boolean isExpired(long cid) throws ProjectException {
        Optional<Confirmation> confirmationOptional = confirmationDao.findEntityById(cid);
        boolean expired;
        if(confirmationOptional.isPresent()){
            expired = Instant.now().isAfter(confirmationOptional.get().getExpiration());
        }
        else{
            throw new ProjectException("Unable to find confirmation with id: " + cid);
        }
        return expired;
    }
}
