package com.epam.likeit.tag;

import com.epam.likeit.bean.UserBean;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.entity.UserStatus;
import lombok.extern.log4j.Log4j2;

import javax.servlet.jsp.tagext.TagSupport;

/**
 * Custom tag, displays its content only if current user has status Available
 */
@Log4j2
public class AvailableUserTag extends TagSupport {
    @Override
    public int doStartTag(){
        boolean evaluateBody = false;
        Object userObject = pageContext.getSession().getAttribute(RequestParamAttr.USER);
        if(userObject != null && userObject instanceof UserBean && ((UserBean)userObject).getStatus() == UserStatus.AVAILABLE){
            evaluateBody = true;
        }
        if(evaluateBody){
            return EVAL_BODY_INCLUDE;
        }
        else {
            return SKIP_BODY;
        }
    }
}
