package com.epam.likeit.util.validator;

public interface Validator<T> {
    boolean validate(T t);
}
