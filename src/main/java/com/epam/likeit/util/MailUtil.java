package com.epam.likeit.util;

import com.epam.likeit.bean.ConfirmationBean;
import com.epam.likeit.command.CommandEnum;
import com.epam.likeit.command.RequestParamAttr;
import com.epam.likeit.resource.Const;
import lombok.extern.log4j.Log4j2;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Class, that provides utility methods for mail tasks
 */
@Log4j2
public class MailUtil {
    private static final String PROP_USERNAME = "mail.username";
    private static final String PROP_PASSWORD = "mail.password";
    private static final String PROP_SUBJECT = "mail.subject";
    private static final String PROP_TEXT = "mail.text";
    private static final String PROP_AUTH = "mail.smtp.auth";
    private static final String PROP_START_TLS = "mail.smtp.starttls.enable";
    private static final String PROP_HOST = "mail.smtp.host";
    private static final String PROP_PORT = "mail.smtp.port";
    private static final String URL_PATH_PROP_DELIMITER = "?";
    private static final String URL_PROP_DELIMITER = "&";
    private static final String URL_KEY_VALUE_DELIMITER = "=";
    private static final String CONTENT_TYPE_KEY = "Content-type";
    private static final String CONTENT_TYPE_VALUE = "text/html; charset=UTF-8";

    private static final Properties PROPERTIES;
    private static ResourceBundle mailBoxResourceBundle = ResourceBundle.getBundle(Const.MAIL_BOX);

    private ResourceBundle messageResourceBundle;

    static {
        PROPERTIES = new Properties();
        PROPERTIES.put(PROP_AUTH, mailBoxResourceBundle.getString(PROP_AUTH));
        PROPERTIES.put(PROP_START_TLS, mailBoxResourceBundle.getString(PROP_START_TLS));
        PROPERTIES.put(PROP_HOST, mailBoxResourceBundle.getString(PROP_HOST));
        PROPERTIES.put(PROP_PORT, mailBoxResourceBundle.getString(PROP_PORT));
    }

    public MailUtil(Locale locale) {
        messageResourceBundle = ResourceBundle.getBundle(Const.MAIL_MESSAGE, locale);
    }

    /**
     * Builds link by adding params to base link
     * @param baseLink base of the link, expected part of url to servlet including
     * @param params map of params, each will be included into link
     * @return link in format baseLink[?key1=value1[key2=values2]]
     */
    public String getLink(String baseLink, Map<String, String> params){
        StringBuilder buffer = new StringBuilder(baseLink);
        buffer.append(URL_PATH_PROP_DELIMITER);
        boolean isFirst = true;
        for(String key: params.keySet()){
            if(isFirst){
                isFirst = false;
            }
            else {
                buffer.append(URL_PROP_DELIMITER);
            }
            buffer.append(key);
            buffer.append(URL_KEY_VALUE_DELIMITER);
            buffer.append(params.get(key));
        }
        return buffer.toString();
    }

    /**
     * Builds link from base params, fetched from confirmation
     * @param baseLink base of the link, expected part of url to servlet including
     * @param confirmation confirmation to fetch link params from
     * @return link, built from params
     */
    public String getLink(String baseLink, ConfirmationBean confirmation){
        Map<String, String> params = new HashMap<>();
        params.put(RequestParamAttr.COMMAND, CommandEnum.CONFIRM.name().toLowerCase());
        params.put(RequestParamAttr.CODE, confirmation.getCode());
        params.put(RequestParamAttr.CONFIRMATION_ID, String.valueOf(confirmation.getId()));
        return getLink(baseLink, params);
    }

    /**
     * Tries to send registration confirmation email to specified recipient
     * @param recipient recipient of the letter
     * @param baseLink base of link to be added to default message text
     * @param confirmation confirmation to fetch link params from
     * @return true if letter sent successfully, false otherwise
     */
    public boolean sendRegistrationConfirmationEmail(String recipient, String baseLink, ConfirmationBean confirmation){
        String link = getLink(baseLink, confirmation);
        String text = messageResourceBundle.getString(PROP_TEXT) + "\n" + link;
        return sendEmail(recipient, messageResourceBundle.getString(PROP_SUBJECT), text);
    }

    /**
     * Tries to send email to specified recipient, with specified subject and text
     * @param recipient recipient of the letter
     * @param subject subject of the letter
     * @param text text of the letter
     * @return true if letter sent successfully, false otherwise
     */
    public boolean sendEmail(String recipient, String subject, String text) {
        String username = mailBoxResourceBundle.getString(PROP_USERNAME);
        String password = mailBoxResourceBundle.getString(PROP_PASSWORD);
        Session session = Session.getInstance(PROPERTIES,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
        );
        boolean result = false;
        try {
            InternetHeaders headers = new InternetHeaders();
            headers.addHeader(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            result = true;
        } catch (MessagingException e) {
            log.error(e);
        }
        return result;
    }
}
