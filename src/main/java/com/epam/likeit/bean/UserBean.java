package com.epam.likeit.bean;

import com.epam.likeit.entity.UserStatus;
import lombok.Data;

import java.util.List;

@Data
public class UserBean {
    private long id;
    private String username;
    private String email;
    private int privilegeLevel;
    private UserStatus status;
    private String avatarPath;

    private ConfirmationBean confirmation;

    private long positiveRating;
    private long negativeRating;

    private List<QuestionBean> questions;
    private List<AnswerBean> answers;
}
