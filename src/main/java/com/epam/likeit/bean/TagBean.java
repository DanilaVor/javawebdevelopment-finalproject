package com.epam.likeit.bean;

import lombok.Data;

@Data
public class TagBean {
    private long id;
    private String text;
}
