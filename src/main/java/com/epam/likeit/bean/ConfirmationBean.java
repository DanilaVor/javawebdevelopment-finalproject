package com.epam.likeit.bean;

import lombok.Data;

import java.util.Date;

@Data
public class ConfirmationBean {
    private long id;
    private Date expiration;
    private String code;
}
