package com.epam.likeit.dao;

import com.epam.likeit.dao.impl.*;

/**
 * Concrete implementation of DaoFactory interface, returns Daos for MySQL db
 */
class MySqlDaoFactory extends DaoFactory {
    /**
     * Get concrete implementation of UserDao interface
     * @return concrete implementation of UserDao interface
     * @see MySqlUserDao
     */
    @Override
    public UserDao getUserDao() {
        return new MySqlUserDao();
    }

    /**
     * Get concrete implementation of QuestionDao interface
     * @return concrete implementation of QuestionDao interface
     * @see MySqlQuestionDao
     */
    @Override
    public QuestionDao getQuestionDao() {
        return new MySqlQuestionDao();
    }

    /**
     * Get concrete implementation of AnswerDao interface
     * @return concrete implementation of AnswerDao interface
     * @see MySqlAnswerDao
     */
    @Override
    public AnswerDao getAnswerDao() {
        return new MySqlAnswerDao();
    }

    /**
     * Get concrete implementation of TagDao interface
     * @return concrete implementation of TagDao interface
     * @see MySqlTagDao
     */
    @Override
    public TagDao getTagDao() {
        return new MySqlTagDao();
    }

    /**
     * Get concrete implementation of MarkDao interface
     * @return concrete implementation of MarkDao interface
     * @see MySqlMarkDao
     */
    @Override
    public MarkDao getMarkDao() {
        return new MySqlMarkDao();
    }

    /**
     * Get concrete implementation of ConfirmationDao interface
     * @return concrete implementation of ConfirmationDao interface
     * @see MySqlConfirmationDao
     */
    @Override
    public ConfirmationDao getConfirmationDao() {
        return new MySqlConfirmationDao();
    }
}
