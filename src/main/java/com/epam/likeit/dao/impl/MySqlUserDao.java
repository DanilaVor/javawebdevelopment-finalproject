package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.UserDao;
import com.epam.likeit.entity.User;
import com.epam.likeit.entity.UserRole;
import com.epam.likeit.entity.UserStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlUserDao extends AbstractMySqlDao<Long, User> implements UserDao {
    private static final String COL_ID = "u_id";
    private static final String COL_USERNAME = "u_username";
    private static final String COL_EMAIL = "u_email";
    private static final String COL_PASSWORD_SALT = "u_password_salt";
    private static final String COL_PASSWORD = "u_password";
    private static final String COL_ROLE= "u_role";
    private static final String COL_STATUS = "u_status";
    private static final String COL_AVATAR_PATH = "u_avatar_path";

    private static final String SQL_FIND_ALL_USERS =
            "SELECT `u_id`, `u_username`, `u_email`, `u_password_salt`, `u_password`, `u_role`, " +
                    "`u_status`, `u_avatar_path` FROM `user` ORDER BY `u_id`;";
    private static final String SQL_FIND_USER_BY_ID =
            "SELECT `u_username`, `u_email`, `u_password_salt`, `u_password`, `u_role`, " +
                    "`u_status`, `u_avatar_path` FROM `user` WHERE `u_id`=?;";
    private static final String SQL_CREATE_USER =
            "INSERT INTO `user` (`u_username`, `u_email`, `u_password_salt`, `u_password`, `u_role`, " +
                    "`u_status`, `u_avatar_path`) VALUES (?, ?, ?, ?, ?, ?, ?);";
    private static final String SQL_DELETE_USER_BY_ID =
            "DELETE FROM `user` WHERE `u_id`=?;";
    private static final String SQL_UPDATE_USER_BY_ID =
            "UPDATE `user` SET `u_username`=?, `u_email`=?, `u_password_salt`=?, `u_password`=?, `u_role`=?," +
                    "`u_status`=?, `u_avatar_path`=? WHERE `u_id`=?";
    private static final String SQL_FIND_ENTITY_BY_USERNAME =
            "SELECT `u_id`, `u_email`, `u_password_salt`, `u_password`, `u_role`, "+
                    "`u_status`, `u_avatar_path` FROM `user` WHERE `u_username`=?;";
    private static final String SQL_FIND_ENTITY_BY_EMAIL =
            "SELECT `u_id`, `u_username`, `u_password_salt`, `u_password`, `u_role`, "+
                    "`u_status`, `u_avatar_path` FROM `user` WHERE `u_email`=?;";
    private static final String SQL_FIND_WITH_OFFSET_AND_LIMIT =
            "SELECT `u_id`, `u_username`, `u_email`, `u_password_salt`, `u_password`, `u_role`, `u_status`, " +
                    "`u_avatar_path` FROM `user` ORDER BY `u_id` LIMIT ? OFFSET ?;";;
    private static final String SQL_GET_COUNT =
            "SELECT COUNT(*) FROM `user`; ";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> findAll() throws ProjectException {
        List<User> users = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_USERS);
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong(COL_ID));
                user.setEmail(resultSet.getString(COL_EMAIL));
                user.setUsername(resultSet.getString(COL_USERNAME));
                user.setPassword(resultSet.getString(COL_PASSWORD));
                user.setPasswordSalt(resultSet.getString(COL_PASSWORD_SALT));
                user.setAvatarPath(resultSet.getString(COL_AVATAR_PATH));
                user.setRole(UserRole.valueOf(resultSet.getString(COL_ROLE).toUpperCase()));
                user.setStatus(UserStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findEntityById(Long id) throws ProjectException {
        User user = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_USER_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet =st.executeQuery();
            if(resultSet.next()) {
                user = new User();
                user.setId(id);
                user.setEmail(resultSet.getString(COL_EMAIL));
                user.setUsername(resultSet.getString(COL_USERNAME));
                user.setPassword(resultSet.getString(COL_PASSWORD));
                user.setPasswordSalt(resultSet.getString(COL_PASSWORD_SALT));
                user.setAvatarPath(resultSet.getString(COL_AVATAR_PATH));
                user.setRole(UserRole.valueOf(resultSet.getString(COL_ROLE).toUpperCase()));
                user.setStatus(UserStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_USER_BY_ID);
            st.setLong(1, id);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(User entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, entity.getUsername());
            st.setString(2, entity.getEmail());
            st.setString(3, entity.getPasswordSalt());
            st.setString(4, entity.getPassword());
            st.setString(5, entity.getRole().name());
            st.setString(6, entity.getStatus().name().toLowerCase());
            st.setString(7, entity.getAvatarPath());
            affectedRows = st.executeUpdate();
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(User entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_USER_BY_ID);
            st.setString(1, entity.getUsername());
            st.setString(2, entity.getEmail());
            st.setString(3, entity.getPasswordSalt());
            st.setString(4, entity.getPassword());
            st.setString(5, entity.getRole().name().toLowerCase());
            st.setString(6, entity.getStatus().name().toLowerCase());
            st.setString(7, entity.getAvatarPath());
            st.setLong(8, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findEntityByUsername(String username) throws ProjectException {
        User user = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_ENTITY_BY_USERNAME);
            st.setString(1, username);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong(COL_ID));
                user.setEmail(resultSet.getString(COL_EMAIL));
                user.setUsername(username);
                user.setPassword(resultSet.getString(COL_PASSWORD));
                user.setPasswordSalt(resultSet.getString(COL_PASSWORD_SALT));
                user.setAvatarPath(resultSet.getString(COL_AVATAR_PATH));
                user.setRole(UserRole.valueOf(resultSet.getString(COL_ROLE).toUpperCase()));
                user.setStatus(UserStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findEntityByEmail(String email) throws ProjectException {
        User user = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_ENTITY_BY_EMAIL);
            st.setString(1, email);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong(COL_ID));
                user.setEmail(email);
                user.setUsername(resultSet.getString(COL_USERNAME));
                user.setPassword(resultSet.getString(COL_PASSWORD));
                user.setPasswordSalt(resultSet.getString(COL_PASSWORD_SALT));
                user.setAvatarPath(resultSet.getString(COL_AVATAR_PATH));
                user.setRole(UserRole.valueOf(resultSet.getString(COL_ROLE).toUpperCase()));
                user.setStatus(UserStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> findUsers(long offset, int limit) throws ProjectException {
        List<User> users = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_WITH_OFFSET_AND_LIMIT);
            st.setInt(1, limit);
            st.setLong(2, offset);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong(COL_ID));
                user.setEmail(resultSet.getString(COL_EMAIL));
                user.setUsername(resultSet.getString(COL_USERNAME));
                user.setPassword(resultSet.getString(COL_PASSWORD));
                user.setPasswordSalt(resultSet.getString(COL_PASSWORD_SALT));
                user.setAvatarPath(resultSet.getString(COL_AVATAR_PATH));
                user.setRole(UserRole.valueOf(resultSet.getString(COL_ROLE).toUpperCase()));
                user.setStatus(UserStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCount() throws ProjectException {
        Statement st = null;
        long result;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_GET_COUNT);
            if(resultSet.next()){
                result = resultSet.getInt(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }
}