package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.TagDao;
import com.epam.likeit.entity.Tag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlTagDao extends AbstractMySqlDao<Long, Tag> implements TagDao {
    private static final String COL_ID = "t_id";
    private static final String COL_TEXT = "t_text";

    private static final String SQL_FIND_ALL_TAGS =
            "SELECT `t_id`, `t_text` FROM `tag` ORDER BY `t_id`;";
    private static final String SQL_FIND_TAG_BY_ID =
            "SELECT `t_text` FROM `tag` WHERE `t_id`=?;";
    private static final String SQL_CREATE_TAG =
            "INSERT INTO `tag` (`t_text`) VALUES (?);";
    private static final String SQL_DELETE_TAG_BY_ID =
            "DELETE FROM `tag` WHERE `t_id`=?;";
    private static final String SQL_UPDATE_TAG_BY_ID =
            "UPDATE `tag` SET `t_text`=? WHERE `t_id`=?";
    private static final String SQL_TRY_FIND_TAG_ID =
            "SELECT `t_id` FROM `tag` WHERE `t_text`=?;";
    private static final String SQL_GET_QUESTION_TAGS =
            "SELECT `tag`.`t_id`, `tag`.`t_text` FROM `tag` JOIN `question_m2m_tag` " +
                    "ON `tag`.`t_id`=`question_m2m_tag`.`tag_id` WHERE `question_m2m_tag`.`question_id`=?" +
                    " ORDER BY `t_id`;";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tag> findAll() throws ProjectException {
        List<Tag> tags = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_TAGS);
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(COL_ID));
                tag.setText(resultSet.getString(COL_TEXT));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return tags;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Tag> findEntityById(Long id) throws ProjectException {
        Tag tag = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_TAG_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                tag = new Tag();
                tag.setId(id);
                tag.setText(resultSet.getString(COL_TEXT));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(tag);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_TAG_BY_ID);
            st.setLong(1, id);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(Tag entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_TAG, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, entity.getText());
            affectedRows = st.executeUpdate();
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(Tag entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_TAG_BY_ID);
            st.setString(1, entity.getText());
            st.setLong(2, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean initTagIdByText(Tag entity) throws ProjectException {
        PreparedStatement st = null;
        boolean created = false;
        try {
            st = getConnection().prepareStatement(SQL_TRY_FIND_TAG_ID);
            st.setString(1, entity.getText());
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(COL_ID));
            }else{
                st = getConnection().prepareStatement(SQL_CREATE_TAG, Statement.RETURN_GENERATED_KEYS);
                st.setString(1, entity.getText());
                st.executeUpdate();
                resultSet = st.getGeneratedKeys();
                if(resultSet.next()) {
                    entity.setId(resultSet.getLong(1));
                }
                created = true;
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return created;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tag> findTagsByQuestionId(long questionId) throws ProjectException {
        List<Tag> tags = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_GET_QUESTION_TAGS);
            st.setLong(1, questionId);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(resultSet.getLong(COL_ID));
                tag.setText(resultSet.getString(COL_TEXT));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return tags;
    }
}
