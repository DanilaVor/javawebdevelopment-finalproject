package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.QuestionDao;
import com.epam.likeit.entity.Question;
import com.epam.likeit.entity.QuestionStatus;
import com.epam.likeit.entity.Tag;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Log4j2
public class MySqlQuestionDao extends AbstractMySqlDao<Long, Question> implements QuestionDao {
    private static final String COL_ID = "q_id";
    private static final String COL_CREATION = "q_creation";
    private static final String COL_LAST_EDIT = "q_last_edit";
    private static final String COL_TITLE = "q_title";
    private static final String COL_TEXT = "q_text";
    private static final String COL_STATUS = "q_status";
    private static final String COL_USER_ID = "q_user_id";

    private static final String SQL_FIND_ALL_QUESTIONS =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status`, `q_user_id` FROM `question` " +
                    "ORDER BY `q_id`;";
    private static final String SQL_FIND_QUESTION_BY_ID =
            "SELECT `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status`, `q_user_id` FROM `question` " +
                    "WHERE `q_id`=?;";
    private static final String SQL_CREATE_QUESTION =
            "INSERT INTO `question` (`q_title`, `q_text`, `q_creation`, `q_last_edit`, `q_status`, `q_user_id`) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";
    private static final String SQL_DELETE_QUESTION_BY_ID =
            "DELETE FROM `question` WHERE `q_id`=?;";
    private static final String SQL_UPDATE_QUESTION_BY_ID =
            "UPDATE `question` SET `q_title`=?, `q_text`=?, `q_creation`=?, `q_last_edit`=?, `q_status`=?, " +
                    "`q_user_id`=? WHERE `q_id`=?;";
    private static final String SQL_CREATE_QUESTION_TAG_CONNECTION =
            "INSERT INTO `question_m2m_tag` (`question_id`, `tag_id`) VALUES (?, ?)";
    private static final String SQL_FIND_LATEST_QUESTIONS_WITH_OFFSET =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status`, `q_user_id` " +
                    "FROM `question` ORDER BY `q_last_edit` DESC, `q_id` LIMIT ? OFFSET ?;";

    private static final String SQL_COUNT_BY_NAME_TAGS_OFFSET_START =
            "SELECT COUNT(*) FROM `question` WHERE ";
    private static final String SQL_COUNT_BY_NAME_TAGS_OFFSET_END =
            "`question`.`q_title` LIKE ?; ";
    private static final String SQL_NOT_DELETED_COUNT_BY_NAME_TAGS_OFFSET_END =
            "`question`.`q_title` LIKE ? AND `q_status` != 'DELETED'; ";

    private static final String SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_START =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status`, `q_user_id` " +
                    "FROM `question` WHERE ";
    private static final String SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_MIDDLE =
            "EXISTS (SELECT 1 FROM `tags` WHERE `tags`.`q_id`=`question`.`q_id` AND UPPER(`tags`.`t_text`)=" +
                    "UPPER(?)) AND ";
    private static final String SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_END =
            "`question`.`q_title` LIKE ? ORDER BY `q_last_edit` DESC, `q_id` LIMIT ? OFFSET ?; ";
    private static final String SQL_FIND_NOT_DELETED_LATEST_BY_NAME_TAGS_OFFSET_END =
            "`question`.`q_title` LIKE ? AND `q_status` != 'DELETED' ORDER BY `q_last_edit` DESC, `q_id` " +
                    "LIMIT ? OFFSET ?; ";

    private static final String SQL_DELETE_TAG_CONNECTIONS =
            "DELETE FROM `question_m2m_tag` WHERE `question_id`=?";
    private static final String SQL_GET_COUNT =
            "SELECT COUNT(1) FROM `question`;";

    private static final String SQL_FIND_QUESTIONS_BY_USER_ID =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status` FROM `question` " +
                    "WHERE `q_user_id` = ? ORDER BY `q_id`;";

    private static final String SQL_FIND_NOT_DELETED_QUESTIONS_BY_USER_ID =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status` FROM `question` " +
                    "WHERE `q_user_id` = ? AND `q_status` != 'DELETED' ORDER BY `q_id`;";
    private static final String SQL_FIND_NOT_DELETED_LATEST_QUESTIONS_WITH_OFFSET =
            "SELECT `q_id`, `q_creation`, `q_title`, `q_text`, `q_last_edit`, `q_status`, `q_user_id` " +
                    "FROM `question` WHERE `q_status` != 'DELETED' ORDER BY `q_last_edit` DESC, `q_id` " +
                    "LIMIT ? OFFSET ?;";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findAll() throws ProjectException {
        List<Question> questions = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_QUESTIONS);
            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getLong(COL_ID));
                question.setUserId(resultSet.getLong(COL_USER_ID));
                question.setTitle(resultSet.getString(COL_TITLE));
                question.setText(resultSet.getString(COL_TEXT));
                question.setStatus(QuestionStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                question.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                question.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                questions.add(question);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return questions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Question> findEntityById(Long id) throws ProjectException {
        Question question = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_QUESTION_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                question = new Question();
                question.setId(id);
                question.setUserId(resultSet.getLong(COL_USER_ID));
                question.setTitle(resultSet.getString(COL_TITLE));
                question.setText(resultSet.getString(COL_TEXT));
                question.setStatus(QuestionStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                question.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                question.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(question);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_QUESTION_BY_ID);
            st.setLong(1, id);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(Question entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_QUESTION, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, entity.getTitle());
            st.setString(2, entity.getText());
            st.setLong(3, entity.getCreationDate().toEpochMilli());
            st.setLong(4, entity.getLastEditDate().toEpochMilli());
            st.setString(5, entity.getStatus().name());
            st.setLong(6, entity.getUserId());
            affectedRows = st.executeUpdate();
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(Question entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_QUESTION_BY_ID);
            st.setString(1, entity.getTitle());
            st.setString(2, entity.getText());
            st.setLong(3, entity.getCreationDate().toEpochMilli());
            st.setLong(4, entity.getLastEditDate().toEpochMilli());
            st.setString(5, entity.getStatus().name());
            st.setLong(6, entity.getUserId());
            st.setLong(7, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findQuestionsByUserId(long id) throws ProjectException {
        return findByUserId(id, SQL_FIND_QUESTIONS_BY_USER_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findNotDeletedQuestionsByUserId(long id) throws ProjectException {
        return findByUserId(id, SQL_FIND_NOT_DELETED_QUESTIONS_BY_USER_ID);
    }

    private List<Question> findByUserId(long id, String query) throws ProjectException {
        List<Question> questions = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(query);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getLong(COL_ID));
                question.setUserId(id);
                question.setTitle(resultSet.getString(COL_TITLE));
                question.setText(resultSet.getString(COL_TEXT));
                question.setStatus(QuestionStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                question.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                question.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                questions.add(question);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return questions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findLatestQuestions(long offset, int count) throws ProjectException {
        return findLatestWithOffset(offset, count, SQL_FIND_LATEST_QUESTIONS_WITH_OFFSET);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findLatestNotDeletedQuestions(long offset, int count) throws ProjectException {
        return findLatestWithOffset(offset, count, SQL_FIND_NOT_DELETED_LATEST_QUESTIONS_WITH_OFFSET);
    }

    private List<Question> findLatestWithOffset(long offset, int count, String query) throws ProjectException {
        List<Question> questions = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(query);
            st.setInt(1, count);
            st.setLong(2, offset);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getLong(COL_ID));
                question.setUserId(resultSet.getLong(COL_USER_ID));
                question.setTitle(resultSet.getString(COL_TITLE));
                question.setText(resultSet.getString(COL_TEXT));
                question.setStatus(QuestionStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                question.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                question.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                questions.add(question);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return questions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findLatestQuestions(long offset, int count, String name, Collection<Tag> tags)
            throws ProjectException {
        return findLatestByNameTagsOffset(offset, count, name, tags, SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_END);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> findLatestNotDeletedQuestions(long offset, int count, String name, Collection<Tag> tags)
            throws ProjectException {
        return findLatestByNameTagsOffset(offset, count, name, tags, SQL_FIND_NOT_DELETED_LATEST_BY_NAME_TAGS_OFFSET_END);
    }

    private List<Question> findLatestByNameTagsOffset(long offset, int count, String name, Collection<Tag> tags,
                                                      String endQuery) throws ProjectException {
        List<Question> questions = new ArrayList<>();
        PreparedStatement st = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_START);
            for(Tag ignored : tags){
                stringBuilder.append(SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_MIDDLE);
            }
            stringBuilder.append(endQuery);
            log.debug(stringBuilder.toString());
            st = getConnection().prepareStatement(stringBuilder.toString());
            int i = 0;
            for(Tag tag: tags){
                ++i;
                st.setString(i, tag.getText());
            }
            if(name != null){
                st.setString(i+1, "%" + name + "%");
            }
            else{
                st.setString(i+1, "%");
            }
            st.setInt(i+2, count);
            st.setLong(i+3, offset);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getLong(COL_ID));
                question.setUserId(resultSet.getLong(COL_USER_ID));
                question.setTitle(resultSet.getString(COL_TITLE));
                question.setText(resultSet.getString(COL_TEXT));
                question.setStatus(QuestionStatus.valueOf(resultSet.getString(COL_STATUS).toUpperCase()));
                question.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                question.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                questions.add(question);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return questions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getQuestionsCount(String name, Collection<Tag> tags)
            throws ProjectException {
        return getCountByNameTagsOffset(name, tags, SQL_COUNT_BY_NAME_TAGS_OFFSET_END);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getNotDeletedQuestionsCount(String name, Collection<Tag> tags)
            throws ProjectException {
        return getCountByNameTagsOffset(name, tags, SQL_NOT_DELETED_COUNT_BY_NAME_TAGS_OFFSET_END);
    }

    private long getCountByNameTagsOffset(String name, Collection<Tag> tags, String endQuery)
            throws ProjectException {
        long result;
        PreparedStatement st = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(SQL_COUNT_BY_NAME_TAGS_OFFSET_START);
            for(Tag ignored : tags){
                stringBuilder.append(SQL_FIND_LATEST_BY_NAME_TAGS_OFFSET_MIDDLE);
            }
            stringBuilder.append(endQuery);
            log.debug(stringBuilder.toString());
            st = getConnection().prepareStatement(stringBuilder.toString());
            int i = 0;
            for(Tag tag: tags){
                ++i;
                st.setString(i, tag.getText());
            }
            if(name != null){
                st.setString(i+1, "%" + name + "%");
            }
            else{
                st.setString(i+1, "%");
            }
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                result = resultSet.getLong(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean createQuestionTagConnection(Question entity, long tagId) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_QUESTION_TAG_CONNECTION);
            st.setLong(1, entity.getId());
            st.setLong(2, tagId);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteTagConnections(Question entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_TAG_CONNECTIONS);
            st.setLong(1, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCount() throws ProjectException {
        Statement st = null;
        long result;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_GET_COUNT);
            if(resultSet.next()){
                result = resultSet.getInt(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }
}
