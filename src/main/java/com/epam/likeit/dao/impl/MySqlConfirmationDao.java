package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.ConfirmationDao;
import com.epam.likeit.entity.Confirmation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlConfirmationDao extends AbstractMySqlDao<Long, Confirmation> implements ConfirmationDao {
    private static final String COL_ID = "c_id";
    private static final String COL_EXPIRATION = "c_expiration";
    private static final String COL_CODE = "c_code";
    private static final String COL_USER_ID = "c_user_id";

    private static final String SQL_FIND_ALL_CONFIRMATIONS =
            "SELECT `c_id`, `c_expiration`, `c_code`, `c_user_id` FROM `confirmation` ORDER BY `c_id`;";
    private static final String SQL_FIND_CONFIRMATION_BY_ID =
            "SELECT `c_expiration`, `c_code`, `c_user_id` FROM `confirmation` WHERE `c_id`=?;";
    private static final String SQL_CREATE_CONFIRMATION =
            "INSERT INTO `confirmation` (`c_expiration`, `c_code`, `c_user_id`) VALUES (?, ?, ?);";
    private static final String SQL_DELETE_CONFIRMATION_BY_ID =
            "DELETE FROM `confirmation` WHERE `c_id`=?;";
    private static final String SQL_UPDATE_CONFIRMATION_BY_ID =
            "UPDATE `confirmation` SET `c_expiration`=?, `c_code`=?, `c_user_id`=? WHERE `c_id`=?;";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Confirmation> findAll() throws ProjectException {
        List<Confirmation> confirmations = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_CONFIRMATIONS);
            while (resultSet.next()) {
                Confirmation confirmation = new Confirmation();
                confirmation.setId(resultSet.getLong(COL_ID));
                confirmation.setCode(resultSet.getString(COL_CODE));
                confirmation.setUserId(resultSet.getLong(COL_USER_ID));
                confirmation.setExpiration(Instant.ofEpochMilli(resultSet.getLong(COL_EXPIRATION)));
                confirmations.add(confirmation);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return confirmations;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Confirmation> findEntityById(Long id) throws ProjectException {
        Confirmation confirmation = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_CONFIRMATION_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                confirmation = new Confirmation();
                confirmation.setId(id);
                confirmation.setCode(resultSet.getString(COL_CODE));
                confirmation.setUserId(resultSet.getLong(COL_USER_ID));
                confirmation.setExpiration(Instant.ofEpochMilli(resultSet.getLong(COL_EXPIRATION)));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(confirmation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_CONFIRMATION_BY_ID);
            st.setLong(1, id);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(Confirmation entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_CONFIRMATION, Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, entity.getExpiration().toEpochMilli());
            st.setString(2, entity.getCode());
            st.setLong(3, entity.getUserId());
            affectedRows = st.executeUpdate();
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(Confirmation entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_CONFIRMATION_BY_ID);
            st.setLong(1, entity.getExpiration().toEpochMilli());
            st.setString(2, entity.getCode());
            st.setLong(3, entity.getUserId());
            st.setLong(4, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }
}
