package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.AbstractDao;
import com.epam.likeit.entity.Entity;
import com.epam.likeit.pool.ProxyConnection;
import lombok.extern.log4j.Log4j2;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class, that provides basic methods for mysql daos. All of them must extend it
 * @param <PK> Primary key type
 * @param <T> Type of entity
 */
@Log4j2
public abstract class AbstractMySqlDao<PK, T extends Entity> implements AbstractDao<PK, T>{
    private ProxyConnection connection;

    /**
     * Tries to close passed statement, if its not null
     * @param statement statement to close
     */
    protected void close(Statement statement){
        if(statement != null){
            try{
                statement.close();
            }
            catch (SQLException e){
                log.error("Unable to close statement: ", e);
            }
        }
    }

    /**
     * Returns connection of dao, or throws exception if its null
     * @return connection
     * @throws ProjectException if connection is null
     */
    ProxyConnection getConnection() throws ProjectException {
        if(connection == null){
            log.error("Connection not set for DAO");
            throw new ProjectException("Connection not set for DAO");
        }
        return connection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(ProxyConnection connection) {
        this.connection = connection;
    }
}
