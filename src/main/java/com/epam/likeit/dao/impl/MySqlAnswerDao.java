package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.AnswerDao;
import com.epam.likeit.entity.Answer;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlAnswerDao extends AbstractMySqlDao<Long, Answer> implements AnswerDao {
    private static final String COL_ID = "a_id";
    private static final String COL_CREATION = "a_creation";
    private static final String COL_LAST_EDIT = "a_last_edit";
    private static final String COL_TEXT = "a_text";
    private static final String COL_IS_DELETED = "a_is_deleted";
    private static final String COL_AUTHOR_ID = "a_author_id";
    private static final String COL_QUESTION_ID = "a_question_id";

    private static final String SQL_FIND_ALL_ANSWERS =
            "SELECT `a_id`, `a_creation`, `a_last_edit`, `a_text`, `a_author_id`, `a_question_id`, `a_is_deleted` " +
                    "FROM `answer` ORDER BY `a_id`;";
    private static final String SQL_FIND_ANSWER_BY_ID =
            "SELECT `a_creation`, `a_last_edit`, `a_text`, `a_author_id`, `a_question_id`, `a_is_deleted` " +
                    "FROM `answer` WHERE `a_id`=?;";
    private static final String SQL_CREATE_ANSWER =
            "INSERT INTO `answer` (`a_creation`, `a_last_edit`, `a_text`, `a_author_id`, `a_question_id`, `a_is_deleted`) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";
    private static final String SQL_DELETE_ANSWER_BY_ID =
            "DELETE FROM `answer` WHERE `a_id`=?;";
    private static final String SQL_UPDATE_ANSWER_BY_ID =
            "UPDATE `answer` SET `a_creation`=?, `a_last_edit`=?, `a_text`=?, `a_author_id`=?, `a_question_id`=?, " +
                    "`a_is_deleted`=? WHERE `a_id`=?;";
    private static final String SQL_FIND_ALL_ANSWERS_BY_QUESTION_ID =
            "SELECT `a_id`, `a_creation`, `a_last_edit`, `a_text`, `a_author_id`, `a_is_deleted` FROM `answer` " +
                    "WHERE `a_question_id`=? ORDER BY `a_id`;";
    private static final String SQL_FIND_ALL_ANSWERS_COUNT_BY_QUESTION_ID =
            "SELECT COUNT(*) FROM `answer` WHERE `a_question_id`=?;";
    private static final String SQL_FIND_NOT_DELETED_ANSWERS_BY_QUESTION_ID =
            "SELECT `a_id`, `a_creation`, `a_last_edit`, `a_text`, `a_author_id`, `a_is_deleted` FROM `answer` " +
                    "WHERE `a_question_id`=? AND `a_is_deleted`=0 ORDER BY `a_id`;";
    private static final String SQL_FIND_NOT_DELETED_ANSWERS_COUNT_BY_QUESTION_ID =
            "SELECT COUNT(*) FROM `answer` WHERE `a_question_id`=? AND `a_is_deleted`=0;";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Answer> findAll() throws ProjectException {
        List<Answer> answers = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_ANSWERS);
            while (resultSet.next()) {
                Answer answer = new Answer();
                answer.setId(resultSet.getLong(COL_ID));
                answer.setText(resultSet.getString(COL_TEXT));
                answer.setAuthorId(resultSet.getLong(COL_AUTHOR_ID));
                answer.setQuestionId(resultSet.getLong(COL_QUESTION_ID));
                answer.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                answer.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                answer.setDeleted(resultSet.getInt(COL_IS_DELETED) == 1);
                answers.add(answer);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return answers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Answer> findEntityById(Long id) throws ProjectException {
        Answer answer = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_ANSWER_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                answer = new Answer();
                answer.setId(id);
                answer.setText(resultSet.getString(COL_TEXT));
                answer.setAuthorId(resultSet.getLong(COL_AUTHOR_ID));
                answer.setQuestionId(resultSet.getLong(COL_QUESTION_ID));
                answer.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                answer.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                answer.setDeleted(resultSet.getInt(COL_IS_DELETED) == 1);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(answer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows = 0;
        try {
            st = getConnection().prepareStatement(SQL_DELETE_ANSWER_BY_ID);
            st.setLong(1, id);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(Answer entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_ANSWER, Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, entity.getCreationDate().toEpochMilli());
            st.setLong(2, entity.getLastEditDate().toEpochMilli());
            st.setString(3, entity.getText());
            st.setLong(4, entity.getAuthorId());
            st.setLong(5, entity.getQuestionId());
            st.setInt(6, entity.isDeleted() ? 1 : 0);
            affectedRows = st.executeUpdate();
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(Answer entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_ANSWER_BY_ID);
            st.setLong(1, entity.getCreationDate().toEpochMilli());
            st.setLong(2, entity.getLastEditDate().toEpochMilli());
            st.setString(3, entity.getText());
            st.setLong(4, entity.getAuthorId());
            st.setLong(5, entity.getQuestionId());
            st.setInt(6, entity.isDeleted() ? 1 : 0);
            st.setLong(7, entity.getId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Answer> findAnswersByQuestionId(long id) throws ProjectException {
        List<Answer> answers = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_ALL_ANSWERS_BY_QUESTION_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Answer answer = new Answer();
                answer.setId(resultSet.getLong(COL_ID));
                answer.setText(resultSet.getString(COL_TEXT));
                answer.setAuthorId(resultSet.getLong(COL_AUTHOR_ID));
                answer.setQuestionId(id);
                answer.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                answer.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                answer.setDeleted(resultSet.getInt(COL_IS_DELETED) == 1);
                answers.add(answer);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return answers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Answer> findNotDeletedAnswersByQuestionId(long id) throws ProjectException {
        List<Answer> answers = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_NOT_DELETED_ANSWERS_BY_QUESTION_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Answer answer = new Answer();
                answer.setId(resultSet.getLong(COL_ID));
                answer.setText(resultSet.getString(COL_TEXT));
                answer.setAuthorId(resultSet.getLong(COL_AUTHOR_ID));
                answer.setQuestionId(id);
                answer.setCreationDate(Instant.ofEpochMilli(resultSet.getLong(COL_CREATION)));
                answer.setLastEditDate(Instant.ofEpochMilli(resultSet.getLong(COL_LAST_EDIT)));
                answer.setDeleted(resultSet.getInt(COL_IS_DELETED) == 1);
                answers.add(answer);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return answers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long findAnswersCountByQuestionId(long id) throws ProjectException {
        PreparedStatement st = null;
        long result;
        try {
            st = getConnection().prepareStatement(SQL_FIND_ALL_ANSWERS_COUNT_BY_QUESTION_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                result = resultSet.getLong(1);
            }
            else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long findNotDeletedAnswersCountByQuestionId(long id) throws ProjectException {
        PreparedStatement st = null;
        long result;
        try {
            st = getConnection().prepareStatement(SQL_FIND_NOT_DELETED_ANSWERS_COUNT_BY_QUESTION_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()){
                result = resultSet.getLong(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }
}
