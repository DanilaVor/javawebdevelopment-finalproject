package com.epam.likeit.dao.impl;

import com.epam.likeit.ProjectException;
import com.epam.likeit.dao.MarkDao;
import com.epam.likeit.entity.Mark;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
public class MySqlMarkDao extends AbstractMySqlDao<Long, Mark> implements MarkDao {
    private static final String COL_USER_ID = "m_user_id";
    private static final String COL_ANSWER_ID = "m_answer_id";
    private static final String COL_VALUE = "m_value";

    private static final String SQL_FIND_ALL_MARKS =
            "SELECT `m_user_id`, `m_answer_id`, `m_value` FROM `mark` ORDER BY `m_answer_id`, `m_user_id`;";
    private static final String SQL_FIND_MARK_BY_USER_AND_ANSWER =
            "SELECT `m_value` FROM `mark` WHERE `m_user_id`=? AND `m_answer_id`=?;";
    private static final String SQL_CREATE_MARK =
            "INSERT INTO `mark` (`m_user_id`, `m_answer_id`, `m_value`) VALUES (?, ?, ?);";
    private static final String SQL_DELETE_MARK_BY_USER_AND_ANSWER =
            "DELETE FROM `mark` WHERE `m_user_id`=? AND `m_answer_id`=?;";
    private static final String SQL_UPDATE_MARK_BY_USER_AND_ANSWER =
            "UPDATE `mark` SET `m_value`=? WHERE `m_user_id`=? AND `m_answer_id`=?;";
    private static final String SQL_GET_USER_POSITIVE_RATING =
            "SELECT COUNT(*) FROM `mark` JOIN `answer` ON `mark`.`m_answer_id`=`answer`.`a_id` " +
                    "WHERE `answer`.`a_author_id`=? AND `m_value`=1;";
    private static final String SQL_GET_USER_NEGATIVE_RATING =
            "SELECT COUNT(*) FROM `mark` JOIN `answer` ON `mark`.`m_answer_id`=`answer`.`a_id` " +
                    "WHERE `answer`.`a_author_id`=? AND `m_value`=0;";
    private static final String SQL_GET_ANSWER_POSITIVE_RATING =
            "SELECT COUNT(*) FROM `mark` WHERE `m_answer_id`=? AND `m_value`=1;";
    private static final String SQL_GET_ANSWER_NEGATIVE_RATING =
            "SELECT COUNT(*) FROM `mark` WHERE `m_answer_id`=? AND `m_value`=0;";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Mark> findAll() throws ProjectException {
        List<Mark> marks = new ArrayList<>();
        Statement st = null;
        try {
            st = getConnection().createStatement();
            ResultSet resultSet = st.executeQuery(SQL_FIND_ALL_MARKS);
            while (resultSet.next()) {
                Mark mark = new Mark();
                boolean value = resultSet.getInt(COL_VALUE) == 1;
                mark.setValue(value);
                mark.setUserId(resultSet.getLong(COL_USER_ID));
                mark.setAnswerId(resultSet.getLong(COL_ANSWER_ID));
                marks.add(mark);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return marks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Mark> findEntityById(Long id) throws ProjectException {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Long id) throws ProjectException {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean create(Mark entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_CREATE_MARK);
            st.setLong(1, entity.getUserId());
            st.setLong(2, entity.getAnswerId());
            st.setInt(3, entity.isValue() ? 1 : 0);
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(Mark entity) throws ProjectException {
        PreparedStatement st = null;
        int affectedRows;
        try {
            st = getConnection().prepareStatement(SQL_UPDATE_MARK_BY_USER_AND_ANSWER);
            st.setInt(1, entity.isValue() ? 1 : 0);
            st.setLong(2, entity.getUserId());
            st.setLong(3, entity.getAnswerId());
            affectedRows = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Mark> findEntityByUserAndAnswerId(long userId, long answerId) throws ProjectException {
        Mark mark = null;
        PreparedStatement st = null;
        try {
            st = getConnection().prepareStatement(SQL_FIND_MARK_BY_USER_AND_ANSWER);
            st.setLong(1, userId);
            st.setLong(2, answerId);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                mark = new Mark();
                boolean value = resultSet.getInt(COL_VALUE) == 1;
                mark.setValue(value);
                mark.setAnswerId(answerId);
                mark.setUserId(userId);
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return Optional.ofNullable(mark);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int putMark(Mark mark) throws ProjectException {
        PreparedStatement st = null;
        int result = 0;
        try {
            st = getConnection().prepareStatement(SQL_FIND_MARK_BY_USER_AND_ANSWER);
            st.setLong(1, mark.getUserId());
            st.setLong(2, mark.getAnswerId());
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                log.debug("found mark");
                boolean value = resultSet.getInt(COL_VALUE) == 1;
                if(value != mark.isValue()){
                    log.debug("De another");
                    st = getConnection().prepareStatement(SQL_DELETE_MARK_BY_USER_AND_ANSWER);
                    st.setLong(1, mark.getUserId());
                    st.setLong(2, mark.getAnswerId());
                    int affectedRows = st.executeUpdate();
                    if(affectedRows != 1){
                        throw new ProjectException("Unexpected affected rows num");
                    }
                    result = 0;
                }
                else{
                    log.debug("De same mark");
                    result = mark.isValue() ? 1 : -1;
                }
            }
            else{
                log.debug("didn't find mark");
                st = getConnection().prepareStatement(SQL_CREATE_MARK);
                st.setLong(1, mark.getUserId());
                st.setLong(2, mark.getAnswerId());
                st.setInt(3, mark.isValue() ? 1 : 0);
                int affectedRows = st.executeUpdate();
                if(affectedRows != 1){
                    throw new ProjectException("Unexpected affected rows num");
                }
                result = mark.isValue() ? 1 : -1;
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getPositiveUserRating(long id) throws ProjectException {
        return getUserRating(SQL_GET_USER_POSITIVE_RATING, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getNegativeUserRating(long id) throws ProjectException {
        return getUserRating(SQL_GET_USER_NEGATIVE_RATING, id);
    }

    private long getUserRating(String statement, long id) throws ProjectException {
        PreparedStatement st = null;
        long result = 0;
        try {
            st = getConnection().prepareStatement(statement);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()) {
                result = resultSet.getLong(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getPositiveAnswerRating(long id) throws ProjectException {
        return getAnswerRating(SQL_GET_ANSWER_POSITIVE_RATING, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getNegativeAnswerRating(long id) throws ProjectException {
        return getAnswerRating(SQL_GET_ANSWER_NEGATIVE_RATING, id);
    }

    private long getAnswerRating(String statement, long id) throws ProjectException {
        PreparedStatement st = null;
        long affectedRows = 0;
        try {
            st = getConnection().prepareStatement(statement);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            if(resultSet.next()){
                affectedRows = resultSet.getLong(1);
            }else{
                throw new ProjectException("Result set is empty");
            }
        } catch (SQLException e) {
            throw new ProjectException(e);
        } finally {
            close(st);
        }
        return affectedRows;
    }
}
