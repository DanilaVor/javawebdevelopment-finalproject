package com.epam.likeit.dao;

import com.epam.likeit.entity.Confirmation;

/**
 * Interface, that provides methods for ConfirmationDaos. All concrete ConfirmationDaos must implement it
 */
public interface ConfirmationDao extends AbstractDao<Long, Confirmation>{
}
