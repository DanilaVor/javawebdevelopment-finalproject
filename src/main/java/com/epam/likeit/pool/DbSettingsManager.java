package com.epam.likeit.pool;

import lombok.extern.log4j.Log4j2;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Class for fetching settings for db from properties file
 */
@Log4j2
class DbSettingsManager {
    private static final String DEFAULT_BUNDLE_NAME = "dbSettings";

    static final String URL;
    static final String USER;
    static final String PASSWORD;
    static final int POOL_SIZE;
    static final int CRITICAL_POOL_SIZE;

    static {
        Locale locale = Locale.ENGLISH;
        ResourceBundle myResources;
        try {
            myResources = ResourceBundle.getBundle(DEFAULT_BUNDLE_NAME, locale);
            URL = myResources.getString("url");
            USER = myResources.getString("user");
            PASSWORD = myResources.getString("password");
            POOL_SIZE = Integer.parseInt(myResources.getString("poolSize"));
            CRITICAL_POOL_SIZE = Integer.parseInt(myResources.getString("criticalPoolSize"));
        }
        catch (MissingResourceException e){
            log.fatal("Resource can't be found", e);
            throw new RuntimeException(e);
        }
        catch (NumberFormatException e){
            log.fatal("Resource value format mismatch", e);
            throw new RuntimeException(e);
        }
    }
}
