package com.epam.likeit.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

@EqualsAndHashCode(callSuper = true)
@Data
public class Confirmation extends Entity {
    private long id;
    private Instant expiration;
    private String code;
    private long userId;
}
