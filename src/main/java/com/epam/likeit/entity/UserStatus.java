package com.epam.likeit.entity;

public enum UserStatus {
    BANNED,
    NOT_CONFIRMED,
    AVAILABLE
}
