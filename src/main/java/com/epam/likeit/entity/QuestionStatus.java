package com.epam.likeit.entity;

public enum QuestionStatus {
    OPEN,
    CLOSED,
    DELETED
}
