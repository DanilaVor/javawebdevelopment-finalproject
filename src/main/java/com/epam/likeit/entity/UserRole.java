package com.epam.likeit.entity;

public enum UserRole {
    USER,
    ADMIN,
    SUPERADMIN
}
