﻿-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema like_it
-- -----------------------------------------------------
-- Согласно заданию, сайт называется Like it. На сайте регистрируются пользователи, каждый из них имеет ник, пароль и рейтинг. Пользователи задают вопросы, каждый из которых может иметь произвольное количество тегов. Также пользователи могут отвечать на заданные вопросы и оценивать ответы других пользователей. Могут редактировать свои вопросы, ответы и оценки. Рейтинг пользователя складывается из всех оценок его ответов. 

-- -----------------------------------------------------
-- Schema like_it
--
-- Согласно заданию, сайт называется Like it. На сайте регистрируются пользователи, каждый из них имеет ник, пароль и рейтинг. Пользователи задают вопросы, каждый из которых может иметь произвольное количество тегов. Также пользователи могут отвечать на заданные вопросы и оценивать ответы других пользователей. Могут редактировать свои вопросы, ответы и оценки. Рейтинг пользователя складывается из всех оценок его ответов. 
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `like_it` DEFAULT CHARACTER SET utf8 ;
USE `like_it` ;

-- -----------------------------------------------------
-- Table `like_it`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`user` (
  `u_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `u_username` VARCHAR(32) NOT NULL COMMENT 'User name',
  `u_email` VARCHAR(254) NOT NULL COMMENT 'User email',
  `u_password_salt` CHAR(6) NOT NULL COMMENT 'Salt to secure password',
  `u_password` VARCHAR(32) NOT NULL COMMENT 'Salted and hashed password',
  `u_role` ENUM('user', 'admin', 'superadmin') NOT NULL DEFAULT 'user' COMMENT 'Is user admin flag',
  `u_status` ENUM('available', 'banned', 'not_confirmed') NOT NULL DEFAULT 'not_confirmed' COMMENT '1 - deleted or banned',
  `u_avatar_ext` VARCHAR(64) NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE INDEX `u_username_UNIQUE` (`u_username` ASC),
  UNIQUE INDEX `u_email_UNIQUE` (`u_email` ASC))
ENGINE = InnoDB
COMMENT = 'User';


-- -----------------------------------------------------
-- Table `like_it`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`question` (
  `q_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Question id',
  `q_creation` BIGINT NOT NULL COMMENT 'Question creation date and time',
  `q_last_edit` BIGINT NOT NULL COMMENT 'Question last edit date and time',
  `q_title` VARCHAR(64) NOT NULL COMMENT 'Question title',
  `q_text` TEXT NOT NULL COMMENT 'Question text',
  `q_status` ENUM('open', 'closed', 'deleted') NOT NULL DEFAULT 'open',
  `q_user_id` BIGINT NOT NULL COMMENT 'Author ',
  PRIMARY KEY (`q_id`),
  INDEX `fk_question_user_idx` (`q_user_id` ASC),
  INDEX `Created` (`q_creation` ASC)  COMMENT 'For selecting N latest questions',
  CONSTRAINT `fk_question_user`
    FOREIGN KEY (`q_user_id`)
    REFERENCES `like_it`.`user` (`u_id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
COMMENT = 'Question';


-- -----------------------------------------------------
-- Table `like_it`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`answer` (
  `a_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Answer id',
  `a_creation` BIGINT NOT NULL COMMENT 'Answer creation date and time',
  `a_last_edit` BIGINT NOT NULL COMMENT 'Answer last edit date and time',
  `a_text` VARCHAR(500) NULL COMMENT 'Answer text',
  `a_is_deleted` BIT(1) NOT NULL DEFAULT 0,
  `a_author_id` BIGINT NOT NULL COMMENT 'Answer author',
  `a_question_id` BIGINT NOT NULL COMMENT 'Question',
  PRIMARY KEY (`a_id`),
  INDEX `fk_comment_user1_idx` (`a_author_id` ASC),
  INDEX `fk_comment_question1_idx` (`a_question_id` ASC),
  INDEX `user_time` (`a_is_deleted` ASC, `a_author_id` ASC, `a_creation` ASC)  COMMENT 'To find latest user\'s answers',
  INDEX `is_deleted` (`a_is_deleted` ASC),
  CONSTRAINT `fk_comment_user1`
    FOREIGN KEY (`a_author_id`)
    REFERENCES `like_it`.`user` (`u_id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_comment_question1`
    FOREIGN KEY (`a_question_id`)
    REFERENCES `like_it`.`question` (`q_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Answer';


-- -----------------------------------------------------
-- Table `like_it`.`mark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`mark` (
  `m_user_id` BIGINT NOT NULL COMMENT 'User, who put this mark',
  `m_answer_id` BIGINT NOT NULL COMMENT 'Evaluated answer',
  `m_value` BIT(1) NOT NULL COMMENT 'Mark value, in range [0, 10]',
  INDEX `fk_mark_user1_idx` (`m_user_id` ASC),
  INDEX `fk_mark_comment1_idx` (`m_answer_id` ASC),
  PRIMARY KEY (`m_user_id`, `m_answer_id`),
  CONSTRAINT `fk_mark_user1`
    FOREIGN KEY (`m_user_id`)
    REFERENCES `like_it`.`user` (`u_id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_mark_comment1`
    FOREIGN KEY (`m_answer_id`)
    REFERENCES `like_it`.`answer` (`a_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Mark for answer';


-- -----------------------------------------------------
-- Table `like_it`.`tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`tag` (
  `t_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Tag id',
  `t_text` VARCHAR(32) NOT NULL COMMENT 'Tag text',
  PRIMARY KEY (`t_id`),
  UNIQUE INDEX `t_text_UNIQUE` (`t_text` ASC))
ENGINE = InnoDB
COMMENT = 'Tag';


-- -----------------------------------------------------
-- Table `like_it`.`question_m2m_tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`question_m2m_tag` (
  `question_id` BIGINT NOT NULL COMMENT 'Question id',
  `tag_id` BIGINT NOT NULL COMMENT 'Tag id',
  PRIMARY KEY (`question_id`, `tag_id`),
  INDEX `fk_question_has_tag_tag1_idx` (`tag_id` ASC),
  INDEX `fk_question_has_tag_question1_idx` (`question_id` ASC),
  CONSTRAINT `fk_question_has_tag_question1`
    FOREIGN KEY (`question_id`)
    REFERENCES `like_it`.`question` (`q_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_has_tag_tag1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `like_it`.`tag` (`t_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Question to tag many to many table';


-- -----------------------------------------------------
-- Table `like_it`.`confirmation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`confirmation` (
  `c_id` BIGINT NOT NULL AUTO_INCREMENT,
  `c_expiration` BIGINT NOT NULL,
  `c_code` CHAR(10) NOT NULL,
  `c_user_id` BIGINT NOT NULL,
  PRIMARY KEY (`c_id`),
  INDEX `fk_confirmation_user1_idx` (`c_user_id` ASC),
  CONSTRAINT `fk_confirmation_user1`
    FOREIGN KEY (`c_user_id`)
    REFERENCES `like_it`.`user` (`u_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `like_it` ;

-- -----------------------------------------------------
-- Placeholder table for view `like_it`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`tags` (`q_id` INT, `t_text` INT);

-- -----------------------------------------------------
-- View `like_it`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `like_it`.`tags`;
USE `like_it`;
CREATE  OR REPLACE VIEW `tags`(`q_id`, `t_text`) AS 
	SELECT `question_m2m_tag`.`question_id`, `tag`.`t_text`
    FROM `question_m2m_tag` 
		JOIN `tag` ON `tag`.`t_id`=`question_m2m_tag`.`tag_id`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
