package com.epam.likeit.util;

import com.epam.likeit.resource.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SecurityUtilTest {
    @Test
    void getSaltLengthTest(){
        String salt = SecurityUtil.getSalt();
        Assertions.assertEquals(salt.length(), Configuration.SALT_LENGTH);
    }

    @Test
    void getSaltCharsTest(){
        String salt = SecurityUtil.getSalt();
        for(char c: salt.toCharArray()){
            Assertions.assertTrue(c >= 'a' && c <= 'z');
        }
    }

    @Test
    void getConfirmationCodeLengthTest(){
        String salt = SecurityUtil.getConfirmationCode();
        Assertions.assertEquals(salt.length(), Configuration.CONFIRMATION_CODE_LENGTH);
    }

    @Test
    void getConfirmationCodeCharsTest(){
        String salt = SecurityUtil.getConfirmationCode();
        for(char c: salt.toCharArray()){
            Assertions.assertTrue(c >= 'a' && c <= 'z');
        }
    }


    @Test
    void getRandomStringLengthTest(){
        String salt = SecurityUtil.getRandomString(10);
        Assertions.assertEquals(salt.length(), 10);
    }

    @Test
    void getRandomStringCodeCharsTest(){
        String salt = SecurityUtil.getConfirmationCode();
        for(char c: salt.toCharArray()){
            Assertions.assertTrue(c >= 'a' && c <= 'z');
        }
    }

    @Test
    void md5Test(){
        String testString = "md5TestString";
        String md5 = SecurityUtil.md5(testString);
        Assertions.assertEquals("b1d0d929138856ca136d6b73e44265b1", md5);
    }

    @Test
    void md5SaltTest(){
        String testString = "md5TestString";
        String testSalt = "SALT";
        String md5 = SecurityUtil.md5(testString, testSalt);
        Assertions.assertEquals("99de77b2746939b51124a85b09e91205", md5);
    }
}
