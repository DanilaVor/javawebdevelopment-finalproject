package com.epam.likeit.util;

import com.epam.likeit.resource.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Locale;

public class LocaleUtilTest {
    @Test
    void getLocaleEnglishTest(){
        Locale locale = LocaleUtil.getLocale("en_GB");
        Assertions.assertEquals(Locale.UK, locale);
    }

    @Test
    void getLocaleRussianTest(){
        Locale locale = LocaleUtil.getLocale("ru_RU");
        Assertions.assertEquals("RU", locale.getCountry());
    }

    @Test
    void getLocaleDefaultTest(){
        Locale locale = LocaleUtil.getLocale();
        Assertions.assertEquals(Configuration.DEFAULT_LOCALE_COUNTRY, locale.getCountry());
    }

    @Test
    void getLocaleWrongFormatTest(){
        Locale locale = LocaleUtil.getLocale("That'sWrongFormat");
        Assertions.assertEquals(Configuration.DEFAULT_LOCALE_COUNTRY, locale.getCountry());
    }
}
