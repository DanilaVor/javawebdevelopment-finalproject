package com.epam.likeit.util;

import com.epam.likeit.resource.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class TimeUtilTest {
    @Test
    void getConfirmationExpirationTest(){
        int expirationTime = Configuration.CONFIRMATION_EXPIRATION;
        Instant expected = Instant.now().plus(expirationTime, ChronoUnit.MINUTES);
        Instant expiration = TimeUtil.getConfirmationExpiration();
        long milliDifference = expiration.toEpochMilli() - expected.toEpochMilli();
        Assertions.assertTrue(milliDifference >= 0 && milliDifference <= 20);
    }
}
