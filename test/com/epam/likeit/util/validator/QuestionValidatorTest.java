package com.epam.likeit.util.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuestionValidatorTest {
    @Test
    void validateTextPositiveTest(){
        String validText = "This is random text";
        Assertions.assertTrue(QuestionValidator.validateText(validText));
    }

    @Test
    void validateTextPositiveEmptyTest(){
        String validText = "";
        Assertions.assertTrue(QuestionValidator.validateText(validText));
    }

    @Test
    void validateTextLongNegativeTest(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 1001; ++i){
            sb.append("0123456789");
        }
        Assertions.assertFalse(QuestionValidator.validateText(sb.toString()));
    }

    @Test
    void validateTitlePositiveTest(){
        String validTitle = "This is valid title";
        Assertions.assertTrue(QuestionValidator.validateTitle(validTitle));
    }

    @Test
    void validateTitleEmptyNegativeTest(){
        String invalidTitle = "";
        Assertions.assertFalse(QuestionValidator.validateTitle(invalidTitle));
    }

    @Test
    void validateTitleLongNegativeTest(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 7; ++i){
            sb.append("0123456789");
        }
        Assertions.assertFalse(QuestionValidator.validateTitle(sb.toString()));
    }
}
