package com.epam.likeit.util.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserValidatorTest {
    @Test
    void validateEmailPositiveTest(){
        String validEmail = "testEmail@example.com";
        Assertions.assertTrue(UserValidator.validateEmail(validEmail));
    }

    @Test
    void validateEmailFirstNegativeTest(){
        String invalidEmail = "example text@gmail.com";
        Assertions.assertFalse(UserValidator.validateEmail(invalidEmail));
    }

    @Test
    void validateEmailSecondNegativeTest(){
        String invalidEmail = "@gmail.com";
        Assertions.assertFalse(UserValidator.validateEmail(invalidEmail));
    }

    @Test
    void validateEmailThirdNegativeTest(){
        String invalidEmail = "without_dogtail";
        Assertions.assertFalse(UserValidator.validateEmail(invalidEmail));
    }

    @Test
    void validateUsernameFirstPositiveTest(){
        String validUsername = "JustUsername";
        Assertions.assertTrue(UserValidator.validateUsername(validUsername));
    }

    @Test
    void validateUsernameSecondPositiveTest(){
        String validUsername = "НеАнглийский";
        Assertions.assertTrue(UserValidator.validateUsername(validUsername));
    }

    @Test
    void validateUsernameThirdPositiveTest(){
        String validUsername = "Underscore_Digits_90";
        Assertions.assertTrue(UserValidator.validateUsername(validUsername));
    }

    @Test
    void validateUsernameShortNegativeTest(){
        String invalidUsername = "short";
        Assertions.assertFalse(UserValidator.validateUsername(invalidUsername));
    }

    @Test
    void validateUsernameLongNegativeTest(){
        String invalidUsername = "too_long_username_longer_then_valid";
        Assertions.assertFalse(UserValidator.validateUsername(invalidUsername));
    }

    @Test
    void validateUsernameSpaceNegativeTest(){
        String invalidUsername = "With space";
        Assertions.assertFalse(UserValidator.validateUsername(invalidUsername));
    }

    @Test
    void validateUsernameDotsNegativeTest(){
        String invalidUsername = "With dot. column,";
        Assertions.assertFalse(UserValidator.validateUsername(invalidUsername));
    }

    @Test
    void validatePasswordPositiveTest(){
        String invalidPassword = "ValidPassword";
        Assertions.assertTrue(UserValidator.validatePassword(invalidPassword));
    }

    @Test
    void validatePasswordShortNegativeTest(){
        String invalidPassword = "short";
        Assertions.assertFalse(UserValidator.validatePassword(invalidPassword));
    }

    @Test
    void validatePasswordLongNegativeTest(){
        String invalidPassword = "This_password_is_too_long";
        Assertions.assertFalse(UserValidator.validatePassword(invalidPassword));
    }

    @Test
    void validatePasswordInvalidCharsNegativeTest(){
        String invalidPassword = "Space in password";
        Assertions.assertFalse(UserValidator.validatePassword(invalidPassword));
    }
}
