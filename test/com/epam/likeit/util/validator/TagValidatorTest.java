package com.epam.likeit.util.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TagValidatorTest {
    @Test
    void validateTextPositiveTest(){
        String validTag = "validTag";
        Assertions.assertTrue(TagValidator.validateText(validTag));
    }

    @Test
    void validateTextSpacePositiveTest(){
        String validTag = "thats valid-tag";
        Assertions.assertTrue(TagValidator.validateText(validTag));
    }

    @Test
    void validateTextUnicodePositiveTest(){
        String validTag = "Это тег";
        Assertions.assertTrue(TagValidator.validateText(validTag));
    }

    @Test
    void validateTextLongNegativeTest(){
        String invalidTag = "this tag is toooo long";
        Assertions.assertFalse(TagValidator.validateText(invalidTag));
    }

    @Test
    void validateTextCommaNegativeTest(){
        String invalidTag = "comma,tag";
        Assertions.assertFalse(TagValidator.validateText(invalidTag));
    }
}
