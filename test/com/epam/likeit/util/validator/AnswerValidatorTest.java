package com.epam.likeit.util.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnswerValidatorTest {
    @Test
    void validateTextPositiveTest(){
        String validText = "This is valid answer text";
        Assertions.assertTrue(AnswerValidator.validateText(validText));
    }

    @Test
    void validateTextPositiveUnicodeTest(){
        String validText = "Это валидный текст ответа";
        Assertions.assertTrue(AnswerValidator.validateText(validText));
    }

    @Test
    void validateTextEmptyNegativeTest(){
        String invalidText = "";
        Assertions.assertFalse(AnswerValidator.validateText(invalidText));
    }

    @Test
    void validateTextLongNegativeTest(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 51; ++i){
            sb.append("0123456789");
        }
        Assertions.assertFalse(AnswerValidator.validateText(sb.toString()));
    }
}
