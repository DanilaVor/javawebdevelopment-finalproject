package com.epam.likeit.util;

import com.epam.likeit.bean.TagBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TagUtilTest {
    @Test
    void parseSearchTagsTest(){
        String validTags = "tagOne, tagTwo, tagThree";
        List<TagBean> expected = new ArrayList<>();
        TagBean tag1 = new TagBean();
        tag1.setText("tagOne");
        TagBean tag2 = new TagBean();
        tag2.setText("tagTwo");
        TagBean tag3 = new TagBean();
        tag3.setText("tagThree");
        expected.add(tag1);
        expected.add(tag2);
        expected.add(tag3);
        Assertions.assertIterableEquals(expected, TagUtil.parseSearchTags(validTags, Locale.getDefault()).getTags());
    }

    @Test
    void parseSearchTagsEmptyTest(){
        String validEmpty = "";
        List<TagBean> expected = new ArrayList<>();
        Assertions.assertIterableEquals(expected, TagUtil.parseSearchTags(validEmpty, Locale.getDefault()).getTags());
    }

    @Test
    void parseSearchTagsInvalidDelimiterTest(){
        String invalidTags = "tagOne,, tagTwo, tagThree";
        Assertions.assertNotNull(TagUtil.parseSearchTags(invalidTags, Locale.getDefault()).getErrorMessage());
    }

    @Test
    void parseSearchTagsInvalidTagTest(){
        String invalidTags = "tagOne, tag.Two, tagThree";
        Assertions.assertNotNull(TagUtil.parseSearchTags(invalidTags, Locale.getDefault()).getErrorMessage());
    }

    @Test
    void parseQuestionTagsTest(){
        String validTags = "tagOne, tagTwo, tagThree";
        List<TagBean> expected = new ArrayList<>();
        TagBean tag1 = new TagBean();
        tag1.setText("tagOne");
        TagBean tag2 = new TagBean();
        tag2.setText("tagTwo");
        TagBean tag3 = new TagBean();
        tag3.setText("tagThree");
        expected.add(tag1);
        expected.add(tag2);
        expected.add(tag3);
        Assertions.assertIterableEquals(expected, TagUtil.parseQuestionsTags(validTags, Locale.getDefault()).getTags());
    }

    @Test
    void parseQuestionTagsEmptyTest(){
        String validEmpty = "";
        List<TagBean> expected = new ArrayList<>();
        Assertions.assertNotNull(expected, TagUtil.parseQuestionsTags(validEmpty, Locale.getDefault()).getErrorMessage());
    }

    @Test
    void parseQuestionTagsInvalidDelimiterTest(){
        String invalidTags = "tagOne,, tagTwo, tagThree";
        Assertions.assertNotNull(TagUtil.parseQuestionsTags(invalidTags, Locale.getDefault()).getErrorMessage());
    }

    @Test
    void parseQuestionTagsInvalidTagTest(){
        String invalidTags = "tagOne, tag.Two, tagThree";
        Assertions.assertNotNull(TagUtil.parseQuestionsTags(invalidTags, Locale.getDefault()).getErrorMessage());
    }
}
