package com.epam.likeit.pool;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConnectionPoolTest {
    @Test
    void testTakeConnection(){
        ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
        try {
            Assertions.assertNotNull(proxyConnection);
        }
        finally {
            ConnectionPool.getInstance().returnConnection(proxyConnection);
        }
    }

    @Test
    void testReturnConnection(){
        ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
        Assertions.assertTrue(ConnectionPool.getInstance().returnConnection(proxyConnection));
    }

    @Test
    void testSingleton(){
        Assertions.assertEquals(ConnectionPool.getInstance(), ConnectionPool.getInstance());
    }

    @AfterAll
    static void closePool(){
        ConnectionPool.getInstance().closePool();
    }
}
